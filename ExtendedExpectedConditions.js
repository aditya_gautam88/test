export class ExtendedExpectedConditions {

    static waitForElementPresent(element, timeout = 30000) {
        browser.wait(ExpectedConditions.presenceOf(element), timeout);
    }

    static waitForElementClickable(element, timeout = 30000) {
        browser.wait(ExpectedConditions.elementToBeClickable(element), timeout);
    }

    static waitForElementVisible(element, timeout = 30000) {
        browser.wait(ExpectedConditions.visibilityOf(element), timeout);
    }

    static waitForElementNotVisible(element, timeout = 30000) {
        browser.wait(ExpectedConditions.invisibilityOf(element),timeout);
    }

    static waitForAttributeToContain(element, attribute, str, timeout = 30000) {
        browser.wait(this.attributeContains(element, attribute, str),timeout);
    }

    static waitForAttributeOfElementLocatedByToContain(locator, timeout = 30000, attribute, str) {
        browser.wait(this.attributeContains(element(locator), attribute, str),timeout);
    }

    static isElementDisplayed(element) {
        return element.isPresent().then(present => {
            return present ? element.isDisplayed() : false;
        });
    }

    static isElementLocatedByDisplayed(locator) {
        return element(locator).isPresent().then(present => {
            return present ? element(locator).isDisplayed() : false;
        });
    }

    static attributeContains(element, attribute, str) {
        return element.getAttribute(attribute).then(value => {
            return value.indexOf(str) !== -1
        });
    }
}