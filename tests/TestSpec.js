import {LoginPage} from "../pageobject/LoginPage.js";
let testData = require("../businessobject/TestData.js");;
import {HomePageObject} from "../pageobject/HomePageObject";
import {GridSheetPageObject} from "../pageobject/GridSheetPageObject";


describe("Login to Smartsheet portal", () => {

    it("Login", () => {
        browser.ignoreSynchronization = true;
        new LoginPage().load().then(lp=>{
                return lp.login(testData.LoginData.userName,testData.LoginData.password)
        })
    });
});


describe('Verify the HomePage and Create new sheet',()=>{

    it('Verify the HomePage and delete a column',()=>{
        browser.ignoreSynchronization = true;
        let homePage = new HomePageObject();
        homePage.clickCreateSheet();
        homePage.clickGridSheet();
        homePage.enterSheetName(testData.SheetData.sheetName);
        homePage.clickSubmit();

    });

    it('Delete a coloumn and validate',()=>{
        let gridSheet = new GridSheetPageObject();
        expect(gridSheet.getHeader()).toEqual(testData.SheetData.sheetName);
        gridSheet.clickColumnOptions(testData.SheetData.deleteColumnNumber);
        gridSheet.deleteAColumn();
        gridSheet.clickSave();
        expect(gridSheet.isColumnPresent(testData.SheetData.deleteColumnNumber)).toBe(false);
        expect(gridSheet.getNumberOfColumns()).toEqual(10);
    });

    it('Insert a column and validate',()=>{
        browser.ignoreSynchronization = true;
        let gridSheet = new GridSheetPageObject();
        gridSheet.clickColumnOptions(testData.SheetData.insertColumnNumber);
        gridSheet.insertAColumnLeft().then(iac=>{
            iac.enterColumnName(testData.SheetData.columnName);
            iac.clickSubmit();
        });
        gridSheet.clickSave();
        expect(gridSheet.isColumnPresent(testData.SheetData.columnName)).toBe(true);
        expect(gridSheet.getNumberOfColumns()).toEqual(11);
    });
 });


