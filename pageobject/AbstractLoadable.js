export class AbstractLoadable {

    constructor(isAngularComponent) {
        this.isAngularComponent = isAngularComponent;
    }

    initComponent() {

        console.log("Initializing: " + this.isAngularComponent);

        if(!this.isAngularComponent) {
            browser.waitForAngularEnabled(this.isAngularComponent);
            return this;
        }

        if(this.isAngularComponent) {
            console.log("Waiting for angular");
            return browser.waitForAngularEnabled().then(isEnabled => {
                if(!isEnabled) {
                    throw "Angular did not load"
                } else {
                    return this;
                }
            });
        }
    }
}
