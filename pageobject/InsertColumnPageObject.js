import {AbstractLoadable} from "./AbstractLoadable";
import {ExtendedExpectedConditions} from "../ExtendedExpectedConditions";

// This pageobject deals with the dialog box when trying to insert a column
export class InsertColumnPageObject extends AbstractLoadable {
    constructor(){
        super(false);

        this.columnName = element(by.css('input[tabindex="1"]'));
        this.submitButton = element(by.css('.clsBorderBox.clsButton.clsStandardButton'));
    }

    enterColumnName(columnName){
        ExtendedExpectedConditions.waitForElementPresent(this.columnName);
        return this.columnName.isPresent().then(isPresent=>{
            if(isPresent){
                return this.columnName.sendKeys(columnName);
            } else{
                throw new Error('Column name text box is not present')
            }
        })
    }

    clickSubmit(){
        return this.submitButton.isPresent().then(isPresent=>{
            if(isPresent){
                return this.submitButton.click();
            } else{
                throw new Error('Ok button is not present')
            }
        })
    }


}