import {AbstractLoadable} from "./AbstractLoadable";
import {ExtendedExpectedConditions} from "../ExtendedExpectedConditions";

export class LoginPage extends AbstractLoadable {

    constructor() {
        super(false);

        this.usernameInput = element(by.css('input[type="email"]'));
        this.passwordInput = element(by.css('input[name="loginPassword"]'));
        this.loginButton = element(by.css('input[value="Log In"]'));
        this.continueButton = element(by.css('input[value="Continue"]'));
    }

    load() {
        return browser.get(browser.baseUrl).then(() => {
            return this.initComponent();
        });
    }

    enterUsername(username) {
        ExtendedExpectedConditions.waitForElementPresent(this.usernameInput);
        return this.usernameInput.clear().then(() => {
            return this.usernameInput.sendKeys(username);
        });
    }

    enterPassword(password) {
        ExtendedExpectedConditions.waitForElementPresent(this.passwordInput);
        return this.passwordInput.clear().then(() => {
            return this.passwordInput.sendKeys(password);
        });
    }

    clickLoginButton() {
        return this.loginButton.isPresent().then(isPresent=>{
            if(isPresent){
                return this.loginButton.click()
            }
        })
    }

    clickNextButton(){
        ExtendedExpectedConditions.waitForElementClickable(this.continueButton)
        return browser.actions().mouseMove(this.continueButton).click().perform();
    }

    login(username, password) {
        return this.enterUsername(username).then(() => {
            return this.clickNextButton().then(() => {
                return this.enterPassword(password).then(() => {
                    return this.clickLoginButton()
                });
           });
        });
    }
}