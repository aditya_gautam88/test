import {AbstractLoadable} from "./AbstractLoadable";
import {ExtendedExpectedConditions} from "../ExtendedExpectedConditions";

// This pageobject deals with Home page for creating sheet
export class HomePageObject extends AbstractLoadable {
    constructor(){
        super(false);


        this.closePopUp = element(by.css('.clsButtonContent'));
        this.createSheet = element(by.css('.clsHomeFeatureAdActionLink'));
        this.gridSheet = element(by.css('.clsGalleryTile.clsGalleryTileBuiltIn'));
        this.sheetName = element(by.css('.clsFloatingForm.clsLargeDropShadow')).element(by.css('.clsBorderBox.clsUserEnteredText'));
        this.cancelButton = element(by.css('.clsBorderBox.clsButton.clsNegativeActionButton'));
        this.okButton = element(by.css('.clsBorderBox.clsButton.clsStandardButton'));
    }

    clickClosePopUp() {
        return this.closePopUp.isPresent().then(isPresent => {
            if(isPresent){
                return this.closePopUp.click();
            } else {
                print('No pop up found')
            }
        })
    }

    clickCreateSheet(){
        ExtendedExpectedConditions.waitForElementClickable(this.createSheet);
        return this.createSheet.isPresent().then(isPresent=>{
            if(isPresent){
                return this.createSheet.click().then(()=>{
                    ExtendedExpectedConditions.waitForElementClickable(this.gridSheet);
                })
            } else {
                throw new Error('Create sheet link is not present')
            }
        })

    }

    clickGridSheet(){
        return this.gridSheet.isPresent().then(isPresent=>{
            if(isPresent){
                return this.gridSheet.click();
            } else {
                throw new Error('Grid sheet is not present')
            }
        })
    }

    getSheetName(){
        return this.sheetName.isPresent().then(isPresent=>{
            if(isPresent){
                return this.sheetName.getAttribute('value');
            } else {
                throw new Error('Sheet name text field is not present')
            }
        })
    }

    enterSheetName(name){
        return this.sheetName.isPresent().then(isPresent=>{
            if(isPresent) {
                    return this.sheetName.sendKeys(name)
            } else {
                throw new Error('Sheet name text field is not present')
            }
        })
    }

    clickSubmit(){
        return this.okButton.isPresent().then(isPresent=>{
            if(isPresent){
                return this.okButton.click();
            }
        })
    }
}