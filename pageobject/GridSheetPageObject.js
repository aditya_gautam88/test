import {AbstractLoadable} from "./AbstractLoadable";
import {InsertColumnPageObject} from "./InsertColumnPageObject"
import {ExtendedExpectedConditions} from "../ExtendedExpectedConditions";

// This is pageObject class for interacting with create sheet (Grid)

export class GridSheetPageObject extends AbstractLoadable {
    constructor(){
        super(false);

        this.header = element(by.css('.containerNameWrapper')).element(by.css('.containerName.editable'));
        this.columnNumber = element.all(by.css('.clsStandardMenuText'));
        this.cloumnOptionDropdown = element.all(by.className('clsGMO clsTableHeadingControls'));
        this.options =  by.css('.clsGMO.clsTableHeadingControlsBackground.clsImageRenderingOptimization');
        this.columns = element.all(by.css('.clsGMO.clsTableHeading'));
        this.deleteColumn = element(by.cssContainingText('.clsStandardMenuText','Delete Column'))
        this.insertColumnLeft = element(by.cssContainingText('.clsStandardMenuText','Insert Column Left'))
        this.saveButton = element(by.css('button[data-client-id="tlb-1"]'));
    }

    getHeader(){
        ExtendedExpectedConditions.waitForElementPresent(this.header);
        return this.header.getText();
    }

    clickHeader(){
        return this.header.isPresent().then(isPresent=>{
            if(isPresent){
                return this.header.click();
            } else {
                throw (" Header is not Clickable")
            }
        })
    }


    getNumberOfColumns(){
        return this.columns.count().then(num=>{
            return num;
        })
    }

    // This function right clicks on the column to display all the options.
    clickColumnOptions(columnName) {
        browser.actions()
            .click(element(by.cssContainingText('.clsTableHeadingText', columnName)), protractor.Button.RIGHT)
            .perform();
    }

    deleteAColumn(){
        return this.deleteColumn.click()
    }


    // Returns another pageobject class
    insertAColumnLeft(){
        return this.insertColumnLeft.click().then(()=>{
            return new InsertColumnPageObject()
        })
    }

    isColumnPresent(columnName){
        return element(by.cssContainingText('.clsTableHeadingText', columnName)).isPresent();
    }

    getAllColumnOptions(){
        return this.columnNumber.then(options=>{
            console.log(options.getText());
        })
    }

    clickSave(){
        ExtendedExpectedConditions.waitForElementClickable(this.saveButton, 5000);
        return this.saveButton.isEnabled().then(isEnabled=>{
            if(isEnabled){
                return this.saveButton.click();
            } else {
                throw new Error('Save button is disabled')
            }
        })
    }


}