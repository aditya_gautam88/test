require("babel-register");

exports.config = {

    framework: 'jasmine2',
    rootElement: 'body',
    seleniumServerJar:'./node_modules/protractor/node_modules/webdriver-manager/selenium/selenium-server-standalone-3.14.0.jar',
    chromeDriver: './node_modules/protractor/node_modules/webdriver-manager/selenium/chromedriver_2.42',
    capabilities: {
        browserName: 'chrome',
        acceptSslCerts: true,
        trustAllSSLCertificates: true,
        acceptInsecureCerts:true,
        ACCEPT_SSL_CERTS:true,
        chromeOptions: {
            args: ['--no-sandbox']
        },
    },

    baseUrl: 'https://app.smartsheet.com',
    specs:["tests/TestSpec.js"],

    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 360000,
        isVerbose: true,
        includeStackTrace: true,
    },

    getPageTimeout: 120000,
    allScriptsTimeout: 360000,
    delayBrowserTimeInSeconds: 0,


    onPrepare: function() {
        browser.manage().timeouts().setScriptTimeout(60000);

        require("babel-register");

        let origFn = browser.driver.controlFlow().execute;
        browser.driver.controlFlow().execute = function () {
            let args = arguments;
            origFn.call(browser.driver.controlFlow(), function () {
                return protractor.promise.delayed(this.delayBrowserTimeInSeconds * 100);
            });
            return origFn.apply(browser.driver.controlFlow(), args);
        };


        let getScreenSize = function () {
            return browser.driver.executeScript(function () {
                return {
                    width: window.screen.availWidth,
                    height: window.screen.availHeight
                };
            });
        };

        getScreenSize().then(function (screenSize) {
            browser.driver.manage().window().setSize(screenSize.width, screenSize.height);
        });
    },
};