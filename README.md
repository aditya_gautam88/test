## Running the code

Instruction to setup your machine via command line.

1. Install node.js form here https://nodejs.org/en/download/. Select windows installer.
2. Test your node.js install via running node -v and npm -v
2. Run **npm install** on you command prompt to install all project dependencies.
3. Run **webdriver-manager update** in command prompt

To run the test.
1. Type this on your command line protractor conf.js
2. Follow the troubleshoot instruction if you get selenium error


**Troubleshoot**:
If the code complains about selenium. Please check your where selenium jar file was installed in step 3 above. Copy it to here **./node_modules/protractor/node_modules/webdriver-manager/selenium/** 
It has to be under the same path as specified in the conf.js file.
**conf.js** file is the configuration file for protractor.

If for some reason you still have issue with selenium file please let me know I can send you the jar file and you can manually copy it to the folder.